-- 1.
-- select Beitrag
-- from beitragsklasse
-- where Bezeichnung = 'Familienbeitrag';

-- 2.
-- select count(*) as 'Anzahl aller Veranstaltungen' from veranstaltung;

-- 3.
-- select ort from mitglied where vorname = 'Lara' and name = 'Croft';

-- 4.
-- Select budget from Abteilung where abteilung = "Volleyball";

-- 5.
-- select email, telefon from mitglied where vorname = "Markus" and Name = "Mueller";

-- 6. 
-- Select MitgliedNr, Name, email from mitglied where email like '%gmx%';

-- 7. 
-- select abteilung from abteilung;

-- 8. 
-- select Abteilung from abteilung where budget >=500;

-- 9.
-- select vorname, name, ort from mitglied where ort = 'Enkenbach-Alsenborn' or Ort = 'Kusel';

-- 10.
-- select * from Aufgabe where bezeichnung like '%Weihnacht%'; --> Join oder vernr=1;

-- 11.
-- select * from mitglied order by name asc; -- asc kann weggelassen werden, da dies automatisch gilt. 

-- 12.
-- select vorname, name from mitglied where name like '_e%';

-- 13.
-- select abteilung from abteilung where budget >500 and Budget <1500;

-- 14.
-- select * from veranstaltung where year(Datum) = year(curdate());
-- where datum like '2021';

-- 15.
-- select bezeichnung, dauer from veranstaltung where dauer >"02%";
-- bei hour(dauer)>2 --> werden nicht alle angezeigt.

-- 16.
-- select bezeichnung from aufgabe where NotwPersonenzahl >1 and month(Datum)=12;

-- 17.
-- select name, vorname from mitglied where ort not like 'Kaiserslautern';
-- Alt. where not ort
-- Alt. where ort != 

-- 18.
-- select * from aufgabe where info is NULL or Info='';