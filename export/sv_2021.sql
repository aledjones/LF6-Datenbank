-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server Version:               10.1.10-MariaDB - mariadb.org binary distribution
-- Server Betriebssystem:        Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Exportiere Datenbank Struktur für sv
DROP DATABASE IF EXISTS `sv`;
CREATE DATABASE IF NOT EXISTS `sv` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `sv`;


-- Exportiere Struktur von Tabelle sv.abteilung
DROP TABLE IF EXISTS `abteilung`;
CREATE TABLE IF NOT EXISTS `abteilung` (
  `AbtNr` int(11) NOT NULL AUTO_INCREMENT,
  `Abteilung` varchar(40) DEFAULT NULL,
  `Abteilungsleiter` varchar(50) DEFAULT NULL,
  `Budget` double DEFAULT NULL,
  PRIMARY KEY (`AbtNr`),
  KEY `IX_AbtNr` (`AbtNr`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Exportiere Daten aus Tabelle sv.abteilung: ~5 rows (ungefähr)
DELETE FROM `abteilung`;
/*!40000 ALTER TABLE `abteilung` DISABLE KEYS */;
INSERT INTO `abteilung` (`AbtNr`, `Abteilung`, `Abteilungsleiter`, `Budget`) VALUES
	(1, 'Fussball', 'Stark', 1500),
	(2, 'Volleyball', 'Mueller', 800),
	(3, 'Tanzen', 'Haid', 300),
	(4, 'Handball', 'Maurer', 600),
	(5, 'Turnen', 'Liebermann', 600);
/*!40000 ALTER TABLE `abteilung` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle sv.abtmitglied
DROP TABLE IF EXISTS `abtmitglied`;
CREATE TABLE IF NOT EXISTS `abtmitglied` (
  `AbtMitgID` int(11) NOT NULL AUTO_INCREMENT,
  `AbtNr` int(11) NOT NULL,
  `MitgliedNr` int(11) NOT NULL,
  PRIMARY KEY (`AbtMitgID`),
  KEY `IX_AbtMitgID` (`AbtMitgID`),
  KEY `IX_AbtNr` (`AbtNr`),
  KEY `IX_MitgliedNr` (`MitgliedNr`),
  CONSTRAINT `abtmitglied_ibfk_1` FOREIGN KEY (`AbtNr`) REFERENCES `abteilung` (`AbtNr`),
  CONSTRAINT `abtmitglied_ibfk_2` FOREIGN KEY (`MitgliedNr`) REFERENCES `mitglied` (`MitgliedNr`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Exportiere Daten aus Tabelle sv.abtmitglied: ~13 rows (ungefähr)
DELETE FROM `abtmitglied`;
/*!40000 ALTER TABLE `abtmitglied` DISABLE KEYS */;
INSERT INTO `abtmitglied` (`AbtMitgID`, `AbtNr`, `MitgliedNr`) VALUES
	(1, 1, 28),
	(2, 3, 25),
	(3, 3, 27),
	(4, 3, 28),
	(5, 1, 21),
	(6, 2, 21),
	(7, 2, 27),
	(8, 1, 23),
	(9, 1, 24),
	(10, 2, 22),
	(11, 4, 30),
	(12, 5, 29),
	(13, 5, 31);
/*!40000 ALTER TABLE `abtmitglied` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle sv.aufgabe
DROP TABLE IF EXISTS `aufgabe`;
CREATE TABLE IF NOT EXISTS `aufgabe` (
  `AufgNr` int(11) NOT NULL AUTO_INCREMENT,
  `Bezeichnung` varchar(50) DEFAULT NULL,
  `Datum` date DEFAULT NULL,
  `Beginn` time DEFAULT NULL,
  `Dauer` time DEFAULT NULL,
  `NotwPersonenzahl` int(11) DEFAULT NULL,
  `Ansprechpartner` varchar(50) DEFAULT NULL,
  `VerNr` int(11) NOT NULL,
  `Info` text,
  PRIMARY KEY (`AufgNr`),
  KEY `IX_AufgNr` (`AufgNr`),
  KEY `IX_VerNr` (`VerNr`),
  CONSTRAINT `aufgabe_ibfk_1` FOREIGN KEY (`VerNr`) REFERENCES `veranstaltung` (`VerNr`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Exportiere Daten aus Tabelle sv.aufgabe: ~13 rows (ungefähr)
DELETE FROM `aufgabe`;
/*!40000 ALTER TABLE `aufgabe` DISABLE KEYS */;
INSERT INTO `aufgabe` (`AufgNr`, `Bezeichnung`, `Datum`, `Beginn`, `Dauer`, `NotwPersonenzahl`, `Ansprechpartner`, `VerNr`, `Info`) VALUES
	(1, 'Dekoration', '2021-11-20', '17:00:00', '01:00:00', 3, 'Herr Stark', 1, 'Tannenbaum aufstellen und schmuecken, Tischdekoration mit Kerzen, sonstige Weihnachtsdekoration'),
	(2, 'Getraenke organisieren', '2021-11-19', '18:00:00', '01:00:00', 2, 'Herr Helferlein', 1, ''),
	(3, 'Weihnachtsrede vorbereiten', '2021-11-04', '00:00:00', '00:00:00', 1, 'Herr Stark', 1, ''),
	(4, 'Belegungsplan Halle vorbereiten', '2021-10-04', '00:00:00', '00:00:00', 1, 'Frau Berg', 3, ''),
	(5, 'Sektempfang vorbereiten', '2021-12-31', '15:00:00', '01:00:00', 2, 'Frau Croft', 2, ''),
	(6, 'Turnierplanung Hallenbelegung', '2021-11-12', '00:00:00', '00:00:00', 1, 'Herr Stark', 4, ''),
	(7, 'Neujahrsrede gestalten', '2021-11-04', '00:00:00', '00:00:00', 2, 'Frau Croft', 2, NULL),
	(8, 'Einladung Weihnachtsfeier', '2021-11-05', '00:00:00', '00:00:00', 1, 'Herr Stark', 1, NULL),
	(9, 'Buffet Weihnachtsfeier organisieren', '2021-11-04', '00:00:00', '00:00:00', 6, 'Frau Berg', 1, ''),
	(10, 'Bewirtung', '2021-11-12', '00:00:00', '00:00:00', 10, 'Herr Koch', 4, NULL),
	(11, 'Weihnachtsgeschenke bestellen', '2021-11-30', '00:00:00', '02:00:00', 1, 'Herr Liebermann', 1, NULL),
	(12, 'Aufraeumen', '2021-12-17', '12:00:00', '03:00:00', 3, 'Herr Rehm', 1, NULL),
	(13, 'Weihnachtsbaum abschmuecken', '2022-01-05', '18:00:00', '02:30:00', 3, 'Herr Schneider', 1, NULL);
/*!40000 ALTER TABLE `aufgabe` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle sv.aufgmitglied
DROP TABLE IF EXISTS `aufgmitglied`;
CREATE TABLE IF NOT EXISTS `aufgmitglied` (
  `AufgMitgID` int(11) NOT NULL AUTO_INCREMENT,
  `AufgNr` int(11) DEFAULT NULL,
  `MitgliedNr` int(11) DEFAULT NULL,
  PRIMARY KEY (`AufgMitgID`),
  KEY `IX_AufgMitgID` (`AufgMitgID`),
  KEY `IX_AufgNr` (`AufgNr`),
  KEY `IX_MitgliedNr` (`MitgliedNr`),
  CONSTRAINT `aufgmitglied_ibfk_1` FOREIGN KEY (`AufgNr`) REFERENCES `aufgabe` (`AufgNr`),
  CONSTRAINT `aufgmitglied_ibfk_2` FOREIGN KEY (`MitgliedNr`) REFERENCES `mitglied` (`MitgliedNr`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Exportiere Daten aus Tabelle sv.aufgmitglied: ~13 rows (ungefähr)
DELETE FROM `aufgmitglied`;
/*!40000 ALTER TABLE `aufgmitglied` DISABLE KEYS */;
INSERT INTO `aufgmitglied` (`AufgMitgID`, `AufgNr`, `MitgliedNr`) VALUES
	(1, 1, 28),
	(2, 1, 27),
	(3, 1, 21),
	(4, 4, 22),
	(5, 3, 25),
	(6, 5, 27),
	(7, 5, 22),
	(8, 9, 22),
	(9, 9, 23),
	(10, 9, 25),
	(11, 9, 26),
	(12, 12, NULL),
	(13, 11, NULL);
/*!40000 ALTER TABLE `aufgmitglied` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle sv.beitragsklasse
DROP TABLE IF EXISTS `beitragsklasse`;
CREATE TABLE IF NOT EXISTS `beitragsklasse` (
  `Klasse` int(11) NOT NULL,
  `Bezeichnung` varchar(30) DEFAULT NULL,
  `Beitrag` double DEFAULT NULL,
  `Bemerkung` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`Klasse`),
  KEY `IX_Klasse` (`Klasse`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportiere Daten aus Tabelle sv.beitragsklasse: ~6 rows (ungefähr)
DELETE FROM `beitragsklasse`;
/*!40000 ALTER TABLE `beitragsklasse` DISABLE KEYS */;
INSERT INTO `beitragsklasse` (`Klasse`, `Bezeichnung`, `Beitrag`, `Bemerkung`) VALUES
	(1, 'Einzelne Person', 55, 'Beitrag erhoeht sich im naechsten Jahr'),
	(2, 'Familienbeitrag', 70, 'Beitrag ohne Erhoehung'),
	(3, 'Auszubildende', 40, ''),
	(4, 'Schueler', 30, ''),
	(5, 'Rentner', 45, ''),
	(6, 'Passives Mitglied', 20, '');
/*!40000 ALTER TABLE `beitragsklasse` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle sv.funktion
DROP TABLE IF EXISTS `funktion`;
CREATE TABLE IF NOT EXISTS `funktion` (
  `Funktion` varchar(30) NOT NULL,
  `Beschreibung` text,
  PRIMARY KEY (`Funktion`),
  KEY `IX_Funktion` (`Funktion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportiere Daten aus Tabelle sv.funktion: ~5 rows (ungefähr)
DELETE FROM `funktion`;
/*!40000 ALTER TABLE `funktion` DISABLE KEYS */;
INSERT INTO `funktion` (`Funktion`, `Beschreibung`) VALUES
	('Kassenwart', 'Buchführen über Einnahmen und Ausgaben'),
	('Keine Funktion', 'Hat (noch) keine Funktion im Verein'),
	('Schriftfuehrer', 'Führt Protokoll bei Sitzungen'),
	('VertreterVorstand', 'Vertretung des Vorstands, z. B. im Krankheitsfall'),
	('Vorstand', 'Vertritt den Verein nach außen hin');
/*!40000 ALTER TABLE `funktion` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle sv.mitglied
DROP TABLE IF EXISTS `mitglied`;
CREATE TABLE IF NOT EXISTS `mitglied` (
  `MitgliedNr` int(11) NOT NULL AUTO_INCREMENT,
  `Vorname` varchar(50) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `GebDatum` date DEFAULT NULL,
  `PLZ` int(11) DEFAULT NULL,
  `Ort` varchar(30) DEFAULT NULL,
  `Strasse` varchar(50) DEFAULT NULL,
  `Hausnummer` int(11) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Telefon` varchar(30) DEFAULT NULL,
  `Funktion` varchar(30) DEFAULT NULL,
  `Klasse` int(11) NOT NULL,
  PRIMARY KEY (`MitgliedNr`),
  KEY `IX_MitgliedNr` (`MitgliedNr`),
  KEY `IX_Funktion` (`Funktion`),
  KEY `IX_Klasse` (`Klasse`),
  CONSTRAINT `mitglied_ibfk_1` FOREIGN KEY (`Funktion`) REFERENCES `funktion` (`Funktion`),
  CONSTRAINT `mitglied_ibfk_2` FOREIGN KEY (`Klasse`) REFERENCES `beitragsklasse` (`Klasse`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- Exportiere Daten aus Tabelle sv.mitglied: ~11 rows (ungefähr)
DELETE FROM `mitglied`;
/*!40000 ALTER TABLE `mitglied` DISABLE KEYS */;
INSERT INTO `mitglied` (`MitgliedNr`, `Vorname`, `Name`, `GebDatum`, `PLZ`, `Ort`, `Strasse`, `Hausnummer`, `Email`, `Telefon`, `Funktion`, `Klasse`) VALUES
	(21, 'Markus', 'Mueller', '1958-01-27', 67665, 'Kaiserslautern', 'Friedenstrasse', 48, 'MuellerM48@gmx.de', '0631 23456', 'Schriftfuehrer', 1),
	(22, 'Heidi', 'Berg', '1961-07-19', 67663, 'Kaiserslautern', 'Eisenbahnstrasse', 17, 'Berg89@arcor.de', '0631 350789', 'Vorstand', 5),
	(23, 'Tim', 'Koch', '1992-11-12', 67677, 'Enkenbach-Alsenborn', 'Hauptstrasse', 95, 'TimSuperheld@googlemail.com', '06301 4529', 'Keine Funktion', 3),
	(24, 'Florian', 'Schneider', '1993-05-09', 66864, 'Kusel', 'Wiesenstrasse', 26, 'XXFlorian9@gmx.de', '06381 28748', 'Keine Funktion', 3),
	(25, 'Maren', 'Haid', '1993-10-03', 67661, 'Kaiserslautern', 'Blumenstrasse', 32, 'Haidschnucke@gmx.de', '0631 359281', 'Keine Funktion', 4),
	(26, 'Jan', 'Liebermann', '1956-03-25', 66864, 'Kusel', 'Bergstrasse', 8, 'LJan@t-online.de', '06381 7527', 'VertreterVorstand', 2),
	(27, 'Lara', 'Croft', '1995-02-27', 67663, 'Kaiserslautern', 'Gasstrasse', 51, 'CroftGirl99@gmx.de', '0631 352860', 'Keine Funktion', 4),
	(28, 'Andreas', 'Stark', '1964-02-27', 67663, 'Kaiserslautern', 'Fruehlingsstrasse', 13, 'AndiStarki@googlemail.com', '0631 129350', 'Kassenwart', 2),
	(29, 'Max', 'Liebermann', '1944-11-16', 67663, 'Kaiserslautern', 'Fruehlingsstrasse', 15, 'MaxiMaxi@gmx.com', '0631 1489623', 'Keine Funktion', 5),
	(30, 'Jonas', 'Maurer', '1979-05-28', 67665, 'Trippstadt', 'Ringstrasse', 31, 'JoMau@t-online.de', '06306 129342', 'Keine Funktion', 2),
	(31, 'Maximilian', 'Rehm', '1964-05-18', 67663, 'Hohenecken', 'Burgstrasse', 42, 'Maximlian5Rehm@t-online.de', '0631 459682', 'Keine Funktion', 2);
/*!40000 ALTER TABLE `mitglied` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle sv.veranstaltung
DROP TABLE IF EXISTS `veranstaltung`;
CREATE TABLE IF NOT EXISTS `veranstaltung` (
  `VerNr` int(11) NOT NULL AUTO_INCREMENT,
  `Bezeichnung` varchar(50) DEFAULT NULL,
  `Offiziell` varchar(4) DEFAULT NULL,
  `Datum` date DEFAULT NULL,
  `Beginn` time DEFAULT NULL,
  `Dauer` time DEFAULT NULL,
  `Teilnehmeranzahl` int(11) DEFAULT NULL,
  `GeplanteKosten` double DEFAULT NULL,
  `Ansprechpartner` varchar(50) DEFAULT NULL,
  `Einnahmen` double DEFAULT NULL,
  `Ausgaben` double DEFAULT NULL,
  `AbtNr` int(11) NOT NULL,
  `Budget` double DEFAULT NULL,
  PRIMARY KEY (`VerNr`),
  KEY `IX_VerNr` (`VerNr`),
  KEY `IX_AbtNr` (`AbtNr`),
  CONSTRAINT `veranstaltung_ibfk_1` FOREIGN KEY (`VerNr`) REFERENCES `veranstaltung` (`VerNr`),
  CONSTRAINT `veranstaltung_ibfk_2` FOREIGN KEY (`AbtNr`) REFERENCES `abteilung` (`AbtNr`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Exportiere Daten aus Tabelle sv.veranstaltung: ~6 rows (ungefähr)
DELETE FROM `veranstaltung`;
/*!40000 ALTER TABLE `veranstaltung` DISABLE KEYS */;
INSERT INTO `veranstaltung` (`VerNr`, `Bezeichnung`, `Offiziell`, `Datum`, `Beginn`, `Dauer`, `Teilnehmeranzahl`, `GeplanteKosten`, `Ansprechpartner`, `Einnahmen`, `Ausgaben`, `AbtNr`, `Budget`) VALUES
	(1, 'Weihnachtsfeier', 'ja', '2021-12-16', '19:00:00', '02:00:00', 38, 250, 'Herr Stark', 0, 310, 1, 300),
	(2, 'Neujahrstreffen', 'ja', '2022-01-01', '10:00:00', '02:00:00', 18, 85, 'Herr Mueller', 28, 110, 2, 200),
	(3, 'Terminplanung2022', 'nein', '2021-11-04', '20:00:00', '03:00:00', 8, 0, 'Frau Berg', 0, 0, 3, 50),
	(4, 'Hallenfussballturnier', 'nein', '2021-11-23', '14:00:00', '06:00:00', 80, 130, 'Herr Stark', 47, 129, 1, 150),
	(5, 'Grillfest', 'ja', '2021-09-23', '17:00:00', '04:00:00', 50, 125, 'Herr Rehm', 54, 137, 5, 250),
	(6, 'Herbstwanderung', 'ja', '2021-10-08', '14:00:00', '02:00:00', 23, 0, 'Herr Maurer', 0, 0, 4, 120);
/*!40000 ALTER TABLE `veranstaltung` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
