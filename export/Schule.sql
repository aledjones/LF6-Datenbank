-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server Version:               10.1.37-MariaDB - mariadb.org binary distribution
-- Server Betriebssystem:        Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Exportiere Datenbank Struktur für schule
DROP DATABASE IF EXISTS `schule`;
CREATE DATABASE IF NOT EXISTS `schule` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `schule`;


-- Exportiere Struktur von Tabelle schule.klasse
DROP TABLE IF EXISTS `klasse`;
CREATE TABLE IF NOT EXISTS `klasse` (
  `KlassenID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `AnzahlSchueler` int(2) DEFAULT NULL,
  PRIMARY KEY (`KlassenID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Exportiere Daten aus Tabelle schule.klasse: ~0 rows (ungefähr)
DELETE FROM `klasse`;
/*!40000 ALTER TABLE `klasse` DISABLE KEYS */;
INSERT INTO `klasse` (`KlassenID`, `Name`, `AnzahlSchueler`) VALUES
	(1, 'IT18A', 14),
	(2, 'IT18B', 16);
/*!40000 ALTER TABLE `klasse` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle schule.schueler
DROP TABLE IF EXISTS `schueler`;
CREATE TABLE IF NOT EXISTS `schueler` (
  `SchuelerID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `Vorname` varchar(50) DEFAULT NULL,
  `PLZ` int(5) DEFAULT NULL,
  `tel` int(15) DEFAULT NULL,
  `Ort` varchar(50) DEFAULT NULL,
  `Strasse` varchar(50) DEFAULT NULL,
  `Geburtsdatum` date DEFAULT NULL,
  `KlassenID_FK` int(11) DEFAULT NULL,
  PRIMARY KEY (`SchuelerID`),
  KEY `KlassenID_FK` (`KlassenID_FK`),
  CONSTRAINT `schueler_ibfk_1` FOREIGN KEY (`KlassenID_FK`) REFERENCES `klasse` (`KlassenID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Exportiere Daten aus Tabelle schule.schueler: ~0 rows (ungefähr)
DELETE FROM `schueler`;
/*!40000 ALTER TABLE `schueler` DISABLE KEYS */;
INSERT INTO `schueler` (`SchuelerID`, `Name`, `Vorname`, `PLZ`, `tel`, `Ort`, `Strasse`, `Geburtsdatum`, `KlassenID_FK`) VALUES
	(11, 'Bo', 'Hari', 79350, 123, 'Sexau', 'Puffenweg', '1998-01-01', 1),
	(12, 'Nation', 'Inka', 66887, 456, 'Rammelsbach', 'Puffthal', '1999-02-24', 1),
	(13, 'Gurt', 'Jo', 18184, 78967896, 'Poppendorf', 'Poppenweg', '2000-03-04', 1),
	(14, 'Huana', 'Maria', 27624, 1245, 'Flögeln', 'Flögelnstraße', '1998-10-04', 1),
	(15, 'Pard', 'Leo', 83367, 1467, 'Petting', 'Nackterweg', '2000-06-06', 1),
	(16, 'Pferd', 'Niel', 83104, 8876, 'Tuntenhausen', 'Blümchenweg', '2001-12-04', 2),
	(17, 'Zufall', 'Rainer', 67311, 9975, 'Nackterhof', 'Anziehn-Schnell-Straße', '2000-08-16', 2),
	(18, 'Schlüpfer', 'Rosa', 63589, 9999, 'Linsengericht', 'Eintopfweg', '2002-05-17', 2),
	(19, 'Weiler', 'Rod', 16928, 9090, 'Kuhbier', 'Schnapsgasse', '2000-07-09', 2),
	(20, 'Lissmuß', 'Wanda', 6862, 6748, 'Hundeluft', 'Am Schafstall', '2000-09-24', 2);
/*!40000 ALTER TABLE `schueler` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
