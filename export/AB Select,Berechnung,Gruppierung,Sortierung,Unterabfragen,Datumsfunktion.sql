-- 1.
SELECT SUM(Budget) FROM abteilung;  

-- 2.
SELECT Vorname, Name, GebDatum from mitglied order by MONTH(GebDatum),DAY(GebDatum) ASC limit 1;

-- 3.
select Ort as Unterschiedliche_Wohnorte from mitglied group by Ort;

-- 4.
select COUNT(*) as Mitgliedsanzahl from mitglied where Funktion  = "Keine Funktion";

-- 5.
SELECT Vorname, Name, GebDatum from mitglied where MONTH (GebDatum) = 5;
SELECT Vorname, Name, GebDatum from mitglied where MONTHNAME (GebDatum) = "May";

-- 6.
SELECT Bezeichnung from veranstaltung where MONTH (Datum) = 11;
SELECT Bezeichnung from veranstaltung where MONTHNAME (Datum) = "November";

-- 7. 
SELECT DATEDIFF(Datum, CURDATE()) from veranstaltung where Bezeichnung = "Weihnachtsfeier" and YEAR (Datum) = 2021 ;

-- 8.
SELECT Bezeichnung from veranstaltung where YEAR (Datum) = 2021 and MONTH (Datum) = 11 and DAY (Datum) > 15 OR MONTH (Datum) = 12 and DAY (Datum) < 15;

-- 9.
SELECT HOUR (Dauer) from veranstaltung where Bezeichnung = "Weihnachtsfeier";

-- 10. 
SELECT MONTHNAME(GebDatum) as Monat, COUNT(GebDatum) as Anzahl from mitglied group by MONTH (GebDatum);

-- 11.
SELECT Vorname, Name, GebDatum from mitglied ORDER BY GebDatum DESC limit 1;

-- 12.
select AufgNr as AUFG_ID from aufgmitglied where ISNULL(MitgliedNr);

-- 13.
SELECT Vorname, Name, Strasse, Hausnummer, Ort from mitglied where Funktion = "Kassenwart";

-- 14.
select Vorname, Name from mitglied where Klasse = (SELECT Klasse from beitragsklasse where Bezeichnung = "Familienbeitrag");

-- 15.
select Vorname, Name, (YEAR (CURDATE()))-(YEAR (GebDatum)) as "Alter", GebDatum from mitglied where Funktion = "Vorstand";
SELECT Vorname, Name, YEAR (FROM_DAYS((DATEDIFF(CURDATE(),GebDatum)))) as "Alter", GebDatum from mitglied WHERE Funktion = "Vorstand";

-- 16.
SELECT COUNT(*)*(SELECT Beitrag from beitragsklasse where Bezeichnung = "Auszubildende") as "Jahresbeitrag Auszubildende" from mitglied where Klasse = (SELECT Klasse from beitragsklasse where Bezeichnung = "Auszubildende")