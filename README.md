LF6 Datenbank Projekt
======================

MariaDB Instanz in Docker für Arbeiten in LF6

## Unterverzeichnisse

- import
- export

## Details

Starten mit `docker-compose up -d`.  
"`-d`" lässt den Container im Hintergrund laufen.  
Der Container startet bei schweren Fehlern automatisch neu.  
Der Container startet bei Systemstart *nicht* automatisch, sondern muss immer manuell gestartet werden.  
Stoppen geht mit `docker-compose stop`.  
Um den Container zu löschen, einfach mit `docker-compose down` zerstören.

**ACHTUNG:** "`docker-compose down`" zerstört den Container und seine Daten **vollständig**. Vorher immer ein Dump mit dem dokumentierten Befehl in `export/` schreiben und dann kaputt machen.

`.sql`-Dateien in `import/` werden automatisch, aufsteigend nach Dateinamen auf die erstelle Datenbank angewendet. (-> Import von vorherigen Backups)